# Single Responsiblity Principle #
*An object should only have one reason to change; the longer the file or class, the more difficult it will be to achieve this.*

### Example 1 ###

*A simple example with a class that has the responsibility to execute tasks.*

*Beyond this, it also has the responsibility to log all it's operations. if you change the log mecanism you will have to change this class.*

```csharp
public class DataAccess
{
    public void GetData(DataToFindObjectType dataToFind)
    {
        //Get data from data source
        System.WriteLine($"Retrived data from source for {dataToFind.Key}");

    }
    public void SaveData(DataObjectType dataToSave)
    {
        //Save data into data source
        System.WriteLine("Saved data into source");
    }
}
```
*Removing the log function from the DataAccess class.*

```csharp
public class DataAccess
{
    public void GetData(DataToFindObjectType dataToFind)
    {
        //Get data from data source
        ApplicationLogger.Log($"Retrived data from source for {dataToFind.Key}");

    }
    public void SaveData(DataObjectType dataToSave)
    {
        //Save data into data source
        ApplicationLogger.Log("Saved data into source");
    }
}

public static class ApplicationLogger
{
    public static void Log(string message)
    {
        System.WriteLine($"{message}"):
    }
}
```